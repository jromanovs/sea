//
//  main.cpp
//  sea
//
//  Created by Jurijs Romanovs on 17/04/2021.
//

#include <iostream>
#include "view/console.hpp"
#include "storage/flatfile.hpp"
#include "viewmodels/accounts_vm.hpp"
#include "viewmodels/transactions_vm.hpp"
#include "viewmodels/transaction_templates_vm.hpp"
#include "defaults/defaults_accounts.hpp"
#include "defaults/defaults_templates.hpp"

void welcome(ConsoleView& console) {
    console.message("-------------------------------------------------\n"
                    "--- Welcome to the Self Employed Accounting ! ---\n"
                    "-------------------------------------------------\n"
                    "    For command help press '/h' and hit Enter\n");
}

void help(ConsoleView& console) {
    console.message("-------------------------------------------------\n"
                    "---             Help on commands              ---\n"
                    "-------------------------------------------------\n"
                    "/a   - append a new record\n"
                    "/e   - edit a record\n"
                    "/d   - delete a record\n"
                    "/s   - perform search for transactions\n"
                    "/p   - apply filter\n"
                    "/l   - list of records (default action - use Enter instead)\n"
                    "/lr  - switch to the list of transactions\n"
                    "/la  - switch to the list of accounts\n"
                    "/lt  - switch to the list of templates\n"
                    "/x   - exit from an application\n"
                    "/?   - display list of available inputs\n"
                    "/;   - finish entering values\n"
                    "/q   - cancel editing data\n"
                    "/h   - display help (this screen)\n");
}

int main(int argc, const char * argv[]) {
    ConsoleView console;
    FlatFileStorage storage{"sea.dat"};
    ViewModel::Accounts cvm{ console, storage.get_columns() };
    ViewModel::TransactionTemplates ttvm{ console, storage.get_templates(), cvm };
    ViewModel::Transactions tvm{ console, storage.get_transactions(), cvm, ttvm };
    ViewModel::ViewModel* currentvm{&tvm};
    
    if (!storage.exists()) {
        console.message("-------------------------------------------------\n"
                        "--- WARNING: No database. New dataset created ---");
        load_default_colummns(storage.get_columns());
        load_default_templates(storage.get_templates());
    } else {
        int rc;
        try {
            rc = storage.load();
        } catch (...) {
            rc = -1;
        }
        if ( rc != 0 ) {
            console.message("----------------------------------------------------------\n"
                            "--- ERROR: Loading data failed. Application terminated ---\n"
                            "----------------------------------------------------------");
            return rc;
        }
    }
    
    std::string command;
    welcome(console);
    while ( true ) {
        command = currentvm->get_command();
        if ( !currentvm->handle(command) ) {
            if (command.rfind("/x", 0) == 0) {
                break;
            } else if (command.rfind("/h", 0) == 0) {
                help(console);
            } else if (command.rfind("/lr", 0) == 0) {
                currentvm = &tvm;
                currentvm->handle(command);
            } else if (command.rfind("/lt", 0) == 0) {
                currentvm = &ttvm;
                currentvm->handle(command);
            } else if (command.rfind("/la", 0) == 0) {
                currentvm = &cvm;
                currentvm->handle(command);
            } else {
                console.message("Invalid command");
            }
        }
    }
    console.message("Application closed");
    return 0;
}
