//
//  console.cpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#include "console.hpp"
#include <iostream>
#include <sstream>
#include <regex>
#include <iomanip>
#include <algorithm>

void ConsoleView::message(std::string amessage) {
    std::cout << amessage << std::endl;
}

void ConsoleView::prompt(std::string aprompt) {
    std::cout << aprompt;
}

std::string& ConsoleView::get() {
    std::getline(std::cin, line);
    return line;
}

bool ConsoleView::confirm(std::string amessage) {
    std::string input;
    prompt(amessage);
    do {
        get_text("[y/n] > ", input);
    } while (input.size() == 0 ||
            (input[0] != 'y' && input[0] != 'Y' && input[0] != 'n' && input[0] != 'N'));
    return input[0] == 'y';
}

/* Return true if changed, throw input-complete and input-cancel */
bool ConsoleView::get_text(const std::string& prompt, std::string &value, const std::string& regex) {
    std::string v;
    std::ostringstream p;
    bool result = false;
    
    if (value.length()==0) {
        p << prompt << ": ";
    } else {
        p << prompt << " [" << value <<"]: ";
    }
    
    while ( std::cout << p.str() && std::getline(std::cin, v) ) {
        if (v.empty()) break;
        if (v == "/;") throw ViewModel::InputComplete(false);
        if (v == "/q") throw ViewModel::InputComplete(true);
        if (v == "/?") throw ViewModel::InputLookup();
        if (regex.empty() || std::regex_match(v,std::regex(regex))) {
            value = v;
            result = true;
            break;
        } else {
            message("Invalid input");
        }
    }
    return result;
}

bool ConsoleView::get_int(const std::string& prompt, Model::IntType &value,
                          const std::string& regex) {
    std::string v = std::to_string(value);
    bool change = get_text(prompt, v, regex);
    if (v == "-") {
        if (change) value = 0;
    } else {
        if (change) value = std::stoi(v);
    }
    return change;
}

bool ConsoleView::get_date(const std::string& prompt, Model::YearMonthDay& value) {
    std::string v=Model::ymd2str(value);
    bool change = false;
    
    while(true) {
        change = get_text(prompt, v,  "[0-9]{1,8}");
        if (Model::str2ymd(v, value)) break;
        message("Invalid date input, please use format DD, DDMM, DDMMYY, or DDMMYYYY");
    }
    
    return change;
}

bool ConsoleView::get_float(const std::string& prompt, double& value, const int precision) {
    std::ostringstream p;
    p << std::fixed << std::setprecision(precision) << value;
//    std::string v = std::to_string(value);
    std::string v = p.str();
    bool change = get_text(prompt, v, "[-]?[0-9]+(\\.[0-9]*)?");
    if (change) value = std::stod(v);
    return change;
}

bool ConsoleView::get_text(const std::string& prompt, char* value, const size_t size,
              const std::string& regex) {
    std::string v;
    v.resize(size+1, 0);
    strncpy(&v[0], value, size);
    bool change = get_text(prompt, v, regex);
    if (change) strncpy(value, v.c_str(), size);
    return change;
}

void ConsoleView::table_item(const std::string& text, int width) {
    std::cout << '|';
    std::cout << std::setw(width) << std::setfill(' ') << std::left << text.substr(0,width);
}

void ConsoleView::table_eor() {
    std::cout << '|' << std::endl;
}

void ConsoleView::table_header(const std::vector<std::string> titles,
                                       const std::vector<int> widths) {
    table_footer(widths);

    std::cout << '|';
    int i=0;
    for (auto w : widths) {
        std::cout << std::setw(w) << std::setfill(' ') << std::left << titles[i++];
        std::cout << "|";
    }
    std::cout << std::endl;

    table_footer(widths);
}

void ConsoleView::table_footer(const std::vector<int> widths) {
    std::cout << '+';
    for (auto w : widths) {
        std::cout << std::setw(w) << std::setfill('-') << "";
        std::cout << "+";
    }
    std::cout << std::endl;
}
