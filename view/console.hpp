//
//  console.hpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#ifndef console_hpp
#define console_hpp
#include <vector>

#include "../viewmodels/viewmodel.hpp"
#include "../viewmodels/accounts_vm.hpp"
#include "../viewmodels/transactions_vm.hpp"
#include "../viewmodels/transaction_templates_vm.hpp"

class ConsoleView : public ViewModel::View {
protected:
    std::string line;
public:
    void message(std::string amessage) override;
    void prompt(std::string aprompt) override;
    std::string& get() override;
    bool confirm(std::string amessage) override;
    bool get_text(const std::string& prompt, std::string &value,
                  const std::string& regex = {}) override;
    bool get_int(const std::string& prompt, Model::IntType &value,
                          const std::string& regex = {}) override;
    bool get_date(const std::string& prompt, Model::YearMonthDay& value) override;
    bool get_float(const std::string& prompt, double& value,
                   const int precision = 2) override;
    bool get_text(const std::string& prompt, char* value, const size_t size,
                  const std::string& regex = {}) override;
    void table_item(const std::string& text, int width) override;
    void table_eor() override;
    void table_header(const std::vector<std::string> titles,
                      const std::vector<int> widths) override;
    void table_footer(const std::vector<int> widths) override;
};

#endif /* console_hpp */
