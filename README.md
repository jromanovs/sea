# sea

Self-employed accounting application. Coursework in C++

# Installation

For OSX and Linux (make):

1. run **make** command from a root directory

2. After comiling use: **build/sea**


For Windows (cmake):

1. Clone project using Visual Studio

2. Open CMakeLists.txt

3. Use Build All from the menu
