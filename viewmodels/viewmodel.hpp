//
//  baseviewmodel.hpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#ifndef viewmodel_hpp
#define viewmodel_hpp

#include <string>
#include <vector>
#include <iostream> // move to implementation
#include "../models/model.hpp"

namespace ViewModel {

class StorageRepository {
public:
    virtual ~StorageRepository();
    virtual void save() = 0;
};

class Storage {
protected:
    StorageRepository& owner;
public:
    Storage(StorageRepository& aowner) : owner(aowner) {}
};

class InputComplete : public std::exception {
public:
    bool cancel;
    InputComplete(bool v) { cancel = v; }
};
class InputLookup : public std::exception {};

class View {
public:
    virtual void prompt(std::string aprompt) = 0;
    virtual void message(std::string amessage) = 0;
    virtual std::string& get() = 0;
    virtual bool confirm(std::string amessage) = 0;
    virtual bool get_text(const std::string& prompt,
                          std::string& value,
                          const std::string& regex = {}) = 0;
    virtual bool get_int(const std::string& prompt,
                          Model::IntType& value,
                          const std::string& regex = {}) = 0;
    virtual bool get_date(const std::string& prompt,
                          Model::YearMonthDay& value) = 0;
    virtual bool get_float(const std::string& prompt,
                           double& value,
                           const int precision=2) = 0;
    virtual bool get_text(const std::string& prompt,
                          char* value,
                          const size_t size,
                          const std::string& regex = {}) = 0;
    virtual void table_item(const std::string& text, int width) = 0;
    virtual void table_header(const std::vector<std::string> titles,
                              const std::vector<int> widths) = 0;
    virtual void table_footer(const std::vector<int> widths) = 0;
    virtual void table_eor() = 0;
};

class ViewModel {
protected:
    View& view;
    Storage& storage;
    std::string prompt;
public:
    ViewModel(View& aview, Storage& astorage) : view(aview), storage(astorage), prompt(" >") {}
    virtual bool handle(std::string command);
    virtual std::string& get_command();
    virtual std::string& get_prompt() { return prompt; }
};

std::string money(double value);
std::string rate(double value);

}

#endif /* viewmodel_hpp */
