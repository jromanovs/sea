//
//  baseviewmodel.cpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#include "viewmodel.hpp"
#include <sstream>
#include <iomanip>

namespace ViewModel {

std::string money(double value) {
    std::ostringstream result;
    result << std::fixed << std::setprecision(2) << std::setw(10) << value;
    return result.str();
}

std::string rate(double value) {
    std::ostringstream result;
    result << std::fixed << std::setprecision(6) << std::setw(10) << value;
    return result.str();
}

std::string& ViewModel::get_command() {
    view.prompt(get_prompt());
    return view.get();
}

bool ViewModel::handle(std::string command) {
    return false;
}

StorageRepository::~StorageRepository() {}

}
