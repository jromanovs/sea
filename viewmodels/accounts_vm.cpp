//
//  accounts.cpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#include "accounts_vm.hpp"
#include "../models/account_m.hpp"
#include <cstring>

namespace ViewModel {

void Accounts::show_one(::Model::Account& a) {
    view.table_item(std::to_string(a.pkey), 5);
    view.table_item(a.id, 8);
    view.table_item(a.currency, 3);
    view.table_item(a.kind_as_string(), 7);
    view.table_item(a.title, 25);
    view.table_item(a.group, 20);
    view.table_eor();
}

void Accounts::list() {
    view.message("List of accounts");
    show_header();
    for (auto e : storage.select()) {
        show_one(e);
        last_ix = e.pkey;
    }
    show_footer();
}

void Accounts::show(Model::IntType ix) {
    show_header();
    Model::Account a;
    if (storage.find(ix, a)) {
        show_one(a);
        last_ix = a.pkey;
    } else {
        view.message("* * * Not found * * *");
    }
    show_footer();
}

bool Accounts::edit(::Model::Account& a) {
    bool changed = false;
    try {
        changed |= view.get_text("Account", a.id, 8, ".{1,8}");
        changed |= view.get_text("Title", a.title);
        changed |= view.get_text("[M]oney,[D]ebt,[I]ncome,[E]xpense)",
                                 (char*)(&a.kind), 1, "[MDIE]");
        changed |= view.get_text("Title group", a.group);
        changed |= view.get_text("Currency", a.currency, 3, "[A-Z]{3}");
    } catch (const InputComplete& e) {
        return e.cancel ? false : changed;
    }
    return changed;
}

bool Accounts::handle(std::string command) {
    if (command.rfind("/a", 0) == 0) {
        view.message("Add a new account");
        Model::Account account;
        
        if ( edit(account) ) {
            auto ix = storage.insert(account);
            view.message("A new account added");
            show( ix );
        } else {
            view.message(">>> Cancel input a new account <<<");
        }
        return true;
    } else if (command.rfind("/e", 0) == 0) {
        Model::IntType ix=last_ix;
        Model::Account account;
        view.message("Edit an account");
        view.get_int("Enter Nr", ix, "[0-9]+");
        if (storage.find(ix, account)) {
            if ( edit(account) ) {
                storage.update(account);
                view.message(">>> Account updated <<<");
                show( ix );
            } else {
                view.message(">>> Cancel edit an account <<<");
            }
        } else {
            view.message(">>> not found <<<");
        }
        return true;
    } else if (command.rfind("/d", 0) == 0) {
        Model::IntType ix=last_ix;
        Model::Account account;
        view.message("Delete an account");
        view.get_int("Enter Nr", ix, "[0-9]+");
        if (storage.find(ix, account)) {
            show( ix );
            if (view.confirm("Are you sure?")) {
                storage.remove(account);
                view.message(">>> Account deleted <<<");
            }
        } else {
            view.message(">>> not found <<<");
        }
        return true;
    } else if (command == "/l" || command.length() == 0 || command == "/la") {
        prompt = " Accounts > ";
        list();
        return true;
    } else {
        return false;
    }
}

void Accounts::show_header() {
    view.table_header({"#","Id","Cur","Kind","Title","Group"}, {5,8,3,7,25,20});
}

void Accounts::show_footer() {
    view.table_footer({5,8,3,7,25,20});
}

void Accounts::show_lookup() {
    view.table_header({"Id","Cur","Title"}, {8,3,25});
    for (auto& e : storage.select()) {
        view.table_item(e.id, 8);
        view.table_item(e.currency, 3);
        view.table_item(e.title, 25);
        view.table_eor();
    }
    view.table_footer({8,3,25});
}

bool Accounts::found(const Model::AccountId id, Model::Account& value) {
    for (auto& e : storage.select()) {
        if ( strcmp(e.id,id) == 0 ) {
            value = e;
            return true;
        }
    }
    return false;
}

std::vector<std::string>& Accounts::get_list() {
    id_list.clear();
    for (auto& e : storage.select()) {
        id_list.push_back(e.id);
    }
    return id_list;
}

}
