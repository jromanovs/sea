//
//  transactions.hpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#ifndef transactions_vm_hpp
#define transactions_vm_hpp

#include "viewmodel.hpp"
#include "../models/model.hpp"
#include "../models/transaction_m.hpp"
#include "accounts_vm.hpp"
#include "transaction_templates_vm.hpp"

namespace ViewModel {

struct TransactionFilter {
    Model::IntType year;
    Model::IntType month;
    Model::AccountId cid;
    TransactionFilter();
};

struct TransactionSearch {
    Model::YearMonthDay period_begin;
    Model::YearMonthDay period_end;
    std::string pattern;
    Model::AccountId cid;
    double amount_begin = 0;
    double amount_end = 999999;
    TransactionSearch();
};

class TransactionsStorage : public Storage {
public:
    TransactionsStorage(StorageRepository& aowner) : Storage(aowner) {};
    virtual Model::IntType insert(Model::Transaction& transaction) = 0;
    virtual bool update(Model::Transaction& transaction) = 0;
    virtual bool remove(Model::Transaction& transaction) = 0;
    virtual Model::TransactionList& select() = 0;
    virtual Model::TransactionList& select(TransactionSearch& search) = 0;
    virtual Model::TransactionList& select(Model::YearMonthDay ymd) = 0;
    virtual bool find(Model::IntType key, Model::Transaction& transaction) = 0;
};

class Transactions : public ViewModel {
private:
    Model::IntType last_ix;
    TransactionsStorage& storage;
    Accounts& accounts_vm;
    TransactionTemplates& templates_vm;
    TransactionFilter period;
    TransactionSearch pattern;
    bool is_prior_period(Model::Transaction& t);
    bool is_current_period(Model::Transaction& t);
    double sum(std::basic_string<char> &cid, Model::Transaction &e);
    double debet(std::basic_string<char> &cid, Model::Transaction &e);
    double credit(std::basic_string<char> &cid, Model::Transaction &e);
    void show_all_columns(const std::string& rowheader, std::vector<double>& values, bool zero_values=true);
    void search();
    void list();
    void list_details();
    void list_summary();
    void list_month();
    void list_search_results();
    void show(Model::IntType ix);
    bool edit(Model::Transaction& transaction);
    void ask_filter();
    bool get_account(Model::AccountId &value, Model::Account& c);
    bool get_template(Model::TemplateId& value, Model::TransactionTemplate& tt);
    double lookup_rate(Model::YearMonthDay ymd, Model::Currency currency);
    double get_rate(Model::Transaction& t, Model::Currency currency);
public:
    Transactions(View& aview, TransactionsStorage& astorage, Accounts& c,
                 TransactionTemplates& tt);
    bool handle(std::string command) override;
    std::string& get_prompt() override;
};

}

#endif /* transactions_hpp */
