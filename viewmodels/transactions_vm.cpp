//
//  transactions.cpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#include "transactions_vm.hpp"
#include <sstream>
#include <iomanip>
#include <cstring>


namespace ViewModel {

TransactionFilter::TransactionFilter() {
    year = Model::current_year();
    month = Model::current_month();
    cid[0] = 0;
}

TransactionSearch::TransactionSearch() {
    int y=Model::current_year(),m=1,d=1;
    Model::int2ymd(y,m,d,period_begin);
    m=12; d=31;
    Model::int2ymd(y,m,d,period_end);
    std::string pattern;
    amount_begin = 0;
    amount_end = 999999;
}

Transactions::Transactions(View& aview, TransactionsStorage& astorage, Accounts& c, TransactionTemplates& tt) :
ViewModel(aview, astorage), storage(astorage), accounts_vm(c), templates_vm(tt)
{ };

void Transactions::list() {
    if (period.month==0) {
        list_summary();
    } else if (period.cid[0] == 0) {
        list_month();
    } else {
        list_details();
    }
}

void Transactions::list_summary() {
    view.message("Year Summary");
    std::vector<std::string> h{"Month"};
    std::vector<int> w{15};
    std::vector<double> opn;
    std::vector<double> mdeb;
    std::vector<double> mcrd;
    std::vector<double> deb;
    std::vector<double> crd;
    std::vector<double> cls;
    Model::Account c;
    int current_month = 1;
    bool started = false;
    
    for (auto& cid : accounts_vm.get_list()) {
        h.push_back(cid);
        w.push_back(10);
        cls.push_back(0);
        opn.push_back(0);
        deb.push_back(0);
        crd.push_back(0);
        mdeb.push_back(0);
        mcrd.push_back(0);
    }
    
    view.table_header(h, w);
    for (auto& e : storage.select()) {
        if (is_prior_period(e)) {
            int t_index=0;
            for (auto& cid : accounts_vm.get_list()) {
                double value = sum(cid, e);
                cls[t_index] += value;
                opn[t_index] += value;
                t_index++;
            }
        } else if (is_current_period(e)) {
            if (!started) {
                show_all_columns("Opening:", opn);
                view.table_footer(w);
                started = true;
            }
            
            int y,m,d;
            Model::ymd2int(e.ymd,y,m,d);
            while (current_month < m) {
                show_all_columns(Model::month(current_month-1), mdeb, false);
                show_all_columns("             - ", mcrd, false);
                current_month++;
            }
            int t_index=0;
            for (auto& cid : accounts_vm.get_list()) {
                double value = sum(cid,e);
                cls[t_index] += value;
                deb[t_index] += debet(cid, e);
                crd[t_index] += credit(cid, e);
                mdeb[t_index] += debet(cid, e);
                mcrd[t_index] += credit(cid, e);
                t_index++;
            }
            last_ix = e.pkey;
        }
    }
    if (!started) {
        show_all_columns("Opening:", opn);
        view.table_footer(w);
        started = true;
    }
    while (12 >= current_month) {
        show_all_columns(Model::month(current_month-1), mdeb, false);
        show_all_columns("             - ", mcrd, false);
        current_month++;
    }
    view.table_footer(w);
    show_all_columns("Debet:", deb);
    show_all_columns("Credit:", crd);
    show_all_columns("Closing:", cls);
    view.table_footer(w);
}

void Transactions::list_month() {
    view.message("Month Summary");
    std::vector<std::string> h{"#","Date"};
    std::vector<int> w{5,10};
    std::vector<double> opn;
    std::vector<double> deb;
    std::vector<double> crd;
    std::vector<double> cls;
    Model::Account c;
    bool started = false;
    
    for (auto& cid : accounts_vm.get_list()) {
        h.push_back(cid);
        w.push_back(10);
        cls.push_back(0);
        opn.push_back(0);
        deb.push_back(0);
        crd.push_back(0);
    }
    
    view.table_header(h, w);
    for (auto& e : storage.select()) {
        if (is_prior_period(e)) {
            int t_index=0;
            for (auto& cid : accounts_vm.get_list()) {
                double value = sum(cid, e);
                cls[t_index] += value;
                opn[t_index] += value;
                t_index++;
            }
        } else if (is_current_period(e)) {
            if (!started) {
                view.table_item("Opening:", 16);
                for (auto s : opn) {
                    view.table_item(money(s), 10);
                }
                view.table_eor();
                view.table_footer(w);
                started = true;
            }
            
            int t_index=0;
            view.table_item(std::to_string(e.pkey), 5);
            view.table_item(Model::ymd_format(e.ymd), 10);
            for (auto& cid : accounts_vm.get_list()) {
                double value = sum(cid, e);
                cls[t_index] += value;
                deb[t_index] += debet(cid, e);
                crd[t_index] += credit(cid, e);
                if (value==0) {
                    view.table_item("",10);
                } else {
                    view.table_item(money(value), 10);
                }
                t_index++;
            }
            view.table_eor();
            last_ix = e.pkey;
        }
    }
    if (!started) {
        view.table_item("Opening:", 16);
        for (auto s : opn) {
            view.table_item(money(s), 10);
        }
        view.table_eor();
        view.table_footer(w);
        started = true;
    }
    view.table_footer(w);
    view.table_item("Debet:", 16);
    for (auto t : deb) {
        view.table_item(money(t), 10);
    }
    view.table_eor();
    
    view.table_item("Credit:", 16);
    for (auto t : crd) {
        view.table_item(money(t), 10);
    }
    view.table_eor();
    
    view.table_item("Closing:", 16);
    for (auto t : cls) {
        view.table_item(money(t), 10);
    }
    view.table_eor();
    
    view.table_footer(w);
}

void Transactions::list_details() {
    std::vector<std::string> h{"#","Date","Document","Partner","Description"};
    std::vector<int> w{5,10,15,15,15};
    Model::Account c;
    double total = 0, saldo = 0, debet = 0, credit = 0;
    bool started = false;
    if (accounts_vm.found(period.cid, c)) {
        h.push_back(c.id);
        w.push_back(10);
        view.message("List of transactions");
        view.table_header(h, w);
        for (auto& e : storage.select()) {
            if (e.found(period.cid)) {
                if (is_prior_period(e)) {
                    for (auto& o:e.operations) {
                        if (strcmp(c.id,o.aid)==0) {
                            saldo += o.amount;
                            total += o.amount;
                        }
                    }
                } else if (is_current_period(e)) {
                    if (!started) {
                        view.table_item("Opening balance:", 64);
                        view.table_item(money(saldo), 10);
                        view.table_eor();
                        view.table_footer(w);
                        started = true;
                    }
                    view.table_item(std::to_string(e.pkey), 5);
                    view.table_item(Model::ymd_format(e.ymd), 10);
                    view.table_item(e.document, 15);
                    view.table_item(e.partner, 15);
                    view.table_item(e.description, 15);
                    int count=0;
                    for (auto& o : e.operations) {
                        if (strcmp(c.id,o.aid)==0) {
                            if (count>0) {
                                view.table_item("", 64);
                            }
                            view.table_item(money(o.amount), 10);
                            view.table_eor();
                            total += o.amount;
                            if (o.amount>0) debet += o.amount;
                            if (o.amount<0) credit -= o.amount;
                            count++;
                        }
                    }
                    last_ix = e.pkey;
                }
            }
        }
        if (!started) {
            view.table_item("Closing balance:", 64);
            view.table_item(money(saldo), 10);
            view.table_eor();
            view.table_footer(w);
            started = true;
        }
        view.table_footer(w);
        view.table_item("Debet:", 64);
        view.table_item(money(debet), 10);
        view.table_eor();
        
        view.table_item("Credit:", 64);
        view.table_item(money(credit), 10);
        view.table_eor();
        
        view.table_item("Ending balance:", 64);
        view.table_item(money(total), 10);
        view.table_eor();
        view.table_footer(w);
    } else {
        view.message("Wrong column specified for filter");
    }
}

void Transactions::list_search_results() {
    std::vector<std::string> h{"#","Date","Document","Partner","Description","Account","Amount"};
    std::vector<int> w{5,10,15,15,15,10,10};
    
    view.message("Results:");
    view.table_header(h, w);
    for (auto& e : storage.select(pattern)) {
        view.table_item(std::to_string(e.pkey), 5);
        view.table_item(Model::ymd_format(e.ymd), 10);
        view.table_item(e.document, 15);
        view.table_item(e.partner, 15);
        view.table_item(e.description, 15);
        bool first = true;
        for (auto& o : e.operations) {
            if (first) {
                first = false;
            } else {
                view.table_item("", 64);
            }
            view.table_item(o.aid, 10);
            view.table_item(money(o.amount), 10);
            view.table_eor();
        }
        last_ix = e.pkey;
    }
    view.table_footer(w);
}

void Transactions::show(Model::IntType ix) {
    std::vector<std::string> h{"#","Date","Document","Partner","Description"};
    std::vector<int> w{5,10,15,15,15};
    bool print_rates=false;
    Model::Transaction t;
    if (storage.find(ix, t)) {
        for (auto& o: t.operations) {
            Model::Account c;
            h.push_back(o.aid);
            w.push_back(10);
            accounts_vm.found(o.aid, c);
            if (!c.is_default_currency()) {
                print_rates = true;
            }
        }
        view.table_header(h, w);
        
        view.table_item(std::to_string(t.pkey), 5);
        view.table_item(Model::ymd_format(t.ymd), 10);
        view.table_item(t.document, 15);
        view.table_item(t.partner, 15);
        view.table_item(t.description, 15);
        for (auto& o: t.operations) {
            view.table_item(money(o.amount), 10);
        }
        view.table_eor();
        last_ix = t.pkey;
        
        view.table_footer(w);
        if (print_rates) {
            view.table_item("Exchange rates", 64);
            for (auto& o: t.operations) {
                Model::Account c;
                accounts_vm.found(o.aid, c);
                if (!c.is_default_currency()) {
                    view.table_item(rate(o.rate), 10);
                } else {
                    view.table_item("", 10);
                }
            }
            view.table_eor();
            view.table_footer(w);
        }
    } else {
        view.message("* * * Not found * * *");
    }
}

bool Transactions::edit(Model::Transaction& t) {
    bool changed = false;
    try {
        Model::IntType operation_ix=0;
        Model::TransactionTemplate tt;
        changed = get_template(t.tid, tt);
        if (changed) {
            int y,m,d;
            Model::ymd2int(t.ymd, y, m, d);
            d = static_cast<int>(tt.day);
            Model::int2ymd(y, m, d, t.ymd);
            t.document = tt.document;
            t.partner = tt.partner;
            t.description = tt.description;
            std::vector<double> values;
            for (Model::IntType i=0; i < tt.amounts; i++) {
                std::ostringstream header;
                double v = 0;
                if (i < static_cast<Model::IntType>(tt.amount_titles.size()) && i>=0) {
                    view.get_float(tt.amount_titles[i], v, 2);
                } else {
                    view.get_float("Value #"+std::to_string(i+1), v, 2);
                }
                values.push_back(v);
            }
            t.operations.clear();
            bool date_confirmed = false;
            for (auto& to : tt.operations) {
                Model::Operation o{};
                Model::Account a{};
                strncpy(o.aid,to.aid,sizeof o.aid);
                if (accounts_vm.found(o.aid, a)) {
                    if (!a.is_default_currency()) {
                        if (!date_confirmed) {
                            changed |= view.get_date("Confirm date of operation", t.ymd);
                            date_confirmed = true;
                        }
                        o.rate = lookup_rate(t.ymd, a.currency);
                        if (o.rate == 0) {
                            o.rate = get_rate(t, a.currency);
                        }
                        while (o.rate == 0) {
                            view.message("Currency exchange rate not found, but required");
                            changed |= view.get_float("Rate", o.rate, 6);
                        }
                    }
                }
                if (to.amount_nr <= static_cast<Model::IntType>(values.size()) && to.amount_nr>0) {
                    o.amount = round(values[to.amount_nr-1]*to.amount_pct)/100;
                }
                o.amount += to.amount_value;
                if (to.amount_nr < 0) {
                    auto ix = -to.amount_nr-1;
                    if (ix < static_cast<Model::IntType>(t.operations.size())) {
                        o.amount += t.operations[ix].amount_default();
                    }
                }
                t.operations.push_back(o);
            }
        }
        changed |= view.get_date("Date (DD, DDMM, DDMMYY, or DDMMYYYY)", t.ymd);
        changed |= view.get_text("Document", t.document);
        changed |= view.get_text("Partner", t.partner);
        changed |= view.get_text("Description", t.description);
        while (true) {
            Model::Operation o{};
            Model::Account a{};
            if (static_cast<Model::IntType>(t.operations.size()) > operation_ix) {
                o = t.operations[operation_ix];
            }
            changed |= get_account(o.aid,a);
            if (o.aid[0] == 0) break;
            
            changed |= view.get_float("Amount", o.amount, 2);
            if ( strcmp(a.currency, Model::default_currency) != 0 ) {
                do {
                    changed |= view.get_float("Rate", o.rate, 6);
                } while(o.rate==0);
            }
            if (static_cast<Model::IntType>(t.operations.size()) > operation_ix) {
                t.operations[operation_ix] = o;
            } else {
                t.operations.push_back(o);
            }
            operation_ix++;
        }
    } catch (const InputComplete& e) {
        return e.cancel ? false : changed;
    }
    return changed;
}

bool Transactions::handle(std::string command) {
    if (command.rfind("/a", 0) == 0) {
        view.message("Add a new transaction (use /; or empty value in the amount or in the column to finish)");
        Model::Transaction transaction;
        if (Model::current_year() != period.year || Model::current_month() != period.month) {
            if (period.month != 0) {
                Model::int2ymd(static_cast<int>(period.year),
                               static_cast<int>(period.month),
                               1,
                               transaction.ymd);
            } else {
                Model::int2ymd(static_cast<int>(period.year),
                               1,
                               1,
                               transaction.ymd);
            }
        }
        if ( edit(transaction) ) {
            auto ix = storage.insert(transaction);
            view.message(">>> A new transaction added <<<");
            show( ix );
        } else {
            view.message(">>> Input canceled for a new transaction <<<");
        }
        return true;
    } else if (command.rfind("/e", 0) == 0) {
        view.message("Edit a transaction");
        Model::IntType ix=last_ix;
        Model::Transaction transaction;
        view.get_int("Enter Nr", ix, "[0-9]+");
        if (storage.find(ix, transaction)) {
            show( ix );
            if ( edit(transaction) ) {
                storage.update(transaction);
                view.message(">>> Transaction updated <<<");
                show( ix );
            } else {
                view.message(">>> Edit transaction canceled <<<");
            }
        } else {
            view.message(">>> Not found <<<");
        }
        return true;
    } else if (command.rfind("/d", 0) == 0) {
        view.message("Delete a transaction");
        Model::IntType ix=last_ix;
        Model::Transaction transaction;
        view.get_int("Enter Nr", ix, "[0-9]+");
        if (storage.find(ix, transaction)) {
            show( ix );
            if (view.confirm("Are you sure?")) {
                storage.remove(transaction);
                view.message(">>> Transaction deleted <<<");
            }
        } else {
            view.message("not found");
        }
        return true;
    } else if (command == "/l" || command.length() == 0 || command == "/lr") {
        list();
        return true;
    } else if (command.rfind("/f", 0) == 0) {
        ask_filter();
        return true;
    } else if (command.rfind("/s", 0) == 0) {
        search();
        return true;
    } else {
        return false;
    }
}

void Transactions::ask_filter() {
    Model::Account c;
    view.message("Apply filter (use the '-' character to specify empty value)");
    while ( view.get_int("Year", period.year, "[0-9]{2,4}") ) {
        if (period.year > 0 && period.year < 99) period.year += 2000;
        if (period.year > 2000 && period.year < 2100) break;
        view.message("Invalid value for a year. Please specify between 2000 and 2100");
    }
    while (view.get_int("Month", period.month, "([0-9]{1,2}|-)") ){
        if (period.month>=0 && period.month<=12) break;
        view.message("Invalid month specified. Please specify between 1 and 12");
    };
    if (period.month !=0 ) {
        get_account(period.cid, c);
    }
    list();
}

bool Transactions::get_account(Model::AccountId &value, Model::Account& c) {
    bool result=false;
    while (true) {
        try {
            result = view.get_text("Account", value, 8);
            if (value[0] == 0) break;
            if (strcmp(value,"-")==0) {
                value[0] = 0;
            } else {
                if (accounts_vm.found(value, c)) break;
                view.message("Invalid account id (use /? to display list of columns)");
            }
        } catch (const InputLookup&) {
            accounts_vm.show_lookup();
        }
    }
    return result;
}

bool Transactions::get_template(Model::TemplateId &value, Model::TransactionTemplate& tt) {
    bool result=false;
    while (true) {
        try {
            result = view.get_text("Template Id ('/?' to list, '-' to clear)", value, 8);
            if (value[0] == 0) break;
            if (strcmp(value,"-")==0) {
                value[0] = 0;
            } else {
                if (templates_vm.found(value, tt)) break;
                view.message("Invalid template id (use '/?' to display list of available templates)");
            }
        } catch (const InputLookup&) {
            templates_vm.show_lookup();
        }
    }
    return result;
}


std::string& Transactions::get_prompt() {
    std::ostringstream new_prompt;
    if (period.cid[0] != 0) {
        new_prompt << std::setw(4) << std::setfill('0') << period.year << "-"
        << std::setw(2) << std::setfill('0') << period.month << ":" <<period.cid << " > ";
    } else if (period.month != 0) {
        new_prompt << std::setw(4) << std::setfill('0') << period.year << "-"
        << std::setw(2) << std::setfill('0') << period.month << " > ";
    } else {
        new_prompt << std::setw(4) << std::setfill('0') << period.year << " > ";
    }
    prompt = new_prompt.str();
    return prompt;
}

bool Transactions::is_prior_period(Model::Transaction& t) {
    int y,m,d;
    Model::ymd2int(t.ymd, y, m, d);
    if (period.month==0) {
        return y < period.year;
    } else {
        return y*12+m < period.year*12+period.month;
    }
}

bool Transactions::is_current_period(Model::Transaction& t) {
    int y,m,d;
    Model::ymd2int(t.ymd, y, m, d);
    if (period.month==0) {
        return y == period.year;
    } else {
        return y*12+m == period.year*12+period.month;
    }
}

double Transactions::sum(std::basic_string<char> &cid, Model::Transaction &e) {
    Model::Account c;
    double value = 0;
    accounts_vm.found(cid.c_str(), c);
    if (c.is_default_currency()) {
        value = e.sum(cid.c_str());
    } else {
        value = e.sum_rate(cid.c_str());
    }
    return value;
}

double Transactions::debet(std::basic_string<char> &cid, Model::Transaction &e) {
    Model::Account c;
    double value = 0;
    accounts_vm.found(cid.c_str(), c);
    if (c.is_default_currency()) {
        value = e.db(cid.c_str());
    } else {
        value = e.dbr(cid.c_str());
    }
    return value;
}

double Transactions::credit(std::basic_string<char> &cid, Model::Transaction &e) {
    Model::Account c;
    double value = 0;
    accounts_vm.found(cid.c_str(), c);
    if (c.is_default_currency()) {
        value = e.cr(cid.c_str());
    } else {
        value = e.crr(cid.c_str());
    }
    return value;
}

void Transactions::show_all_columns(const std::string& rowheader,
                                    std::vector<double>& values, bool zero_values) {
    Model::IntType size = static_cast<Model::IntType>(accounts_vm.get_list().size());
    view.table_item(rowheader, 15);
    for (auto i = 0; i < size; i++) {
        if (values[i]==0 && !zero_values) {
            view.table_item("",10);
        } else {
            view.table_item(money(values[i]), 10);
            values[i] = 0;
        }
    }
    view.table_eor();
}

void Transactions::search() {
    Model::Account c;
    view.message("Search transactions");
    try {
        get_account(pattern.cid, c);
        view.get_text("Pattern to search", pattern.pattern);
        view.get_date("Begin of period", pattern.period_begin);
        view.get_date("End of period", pattern.period_end);
        view.get_float("Minimum value", pattern.amount_begin);
        view.get_float("Maximum value", pattern.amount_end);
    } catch (const InputComplete& e) {
        if (e.cancel) return;
    }
    list_search_results();
}

double Transactions::lookup_rate(Model::YearMonthDay ymd, Model::Currency currency) {
    for (auto& t : storage.select(ymd)) {
        double result = get_rate(t, currency);
        if (result != 0) return result;
    }
    return 0;
}

double Transactions::get_rate(Model::Transaction& t, Model::Currency currency) {
    for (auto& o : t.operations) {
        Model::Account a;
        if (accounts_vm.found(o.aid, a) && o.rate != 0) {
            if (strcmp(a.currency,currency) == 0) {
                return o.rate;
            }
        }
    }
    return 0;
}


}
