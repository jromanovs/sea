//
//  transaction_templates.cpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#include "transaction_templates_vm.hpp"

namespace ViewModel {

void TransactionTemplates::list() {
    view.message("List of templates");
    view.table_header({"#","Id","Name","Expression"}, {5,8,25,25});
    for (auto tt : storage.select()) {
        view.table_item(std::to_string(tt.pkey), 5);
        view.table_item(tt.id, 8);
        view.table_item(tt.name, 25);
        view.table_item(tt.operations_as_str(), 25);
        view.table_eor();
        last_ix = tt.pkey;
    }
    view.table_footer({5,8,25,25});
}

std::string percent(double value) {
    std::ostringstream result;
    result << std::fixed << std::setprecision(1) << value << "%";
    return result.str();
}

void TransactionTemplates::show(Model::IntType ix) {
    Model::TransactionTemplate tt;
    if (storage.find(ix, tt)) {
        view.table_header({"#","Id","Name","DoM","Document","Partner","Description","NoA","Expression"}, {5,8,15,3,10,10,11,3});
        view.table_item(std::to_string(tt.pkey), 5);
        view.table_item(tt.id, 8);
        view.table_item(tt.name, 15);
        view.table_item(std::to_string(tt.day), 3);
        view.table_item(tt.document, 10);
        view.table_item(tt.partner, 10);
        view.table_item(tt.description, 11);
        view.table_item(std::to_string(tt.amounts), 3);
        view.table_eor();
        view.table_header({"","Account","%","Fixed","Value #"}, {28,10,10,10,10});
        for (auto& ot: tt.operations) {
            view.table_item("", 28);
            view.table_item(ot.aid, 10);
            view.table_item(percent(ot.amount_pct), 10);
            view.table_item(money(ot.amount_value), 10);
            view.table_item(std::to_string(ot.amount_nr), 10);
            view.table_eor();
        }
        view.table_footer({28,10,10,10,10});
        last_ix = tt.pkey;
    } else {
        view.message("* * * Not found * * *");
    }

}

bool TransactionTemplates::edit(Model::TransactionTemplate& tt) {
    bool changed = false;
    try {
        Model::IntType ix=0;
        changed |= view.get_text("Template Id", tt.id, 8, ".{1,8}");
        changed |= view.get_text("Template Name", tt.name);
        changed |= view.get_int("Day in month", tt.day);
        changed |= view.get_text("Document", tt.document);
        changed |= view.get_text("Partner", tt.partner);
        changed |= view.get_text("Description", tt.description);
        changed |= view.get_int("Number of values", tt.amounts);
        auto size = tt.amounts;
        while (size--) {
            std::string title;
            if (static_cast<Model::IntType>(tt.amount_titles.size()) > ix) {
                title = tt.amount_titles[ix];
            }
            changed |= view.get_text("Prompt for amount "+std::to_string(tt.amounts-size), title);
            if (static_cast<Model::IntType>(tt.amount_titles.size()) > ix) {
                tt.amount_titles[ix] = title;
            } else {
                tt.amount_titles.push_back(title);
            }
            ix++;
        }
        ix = 0;
        while (true) {
            Model::OperationTemplate ot{};
            Model::Account c{};
            if (static_cast<Model::IntType>(tt.operations.size()) > ix) {
                ot = tt.operations[ix];
            }
            changed |= get_account(ot.aid,c);
            if (ot.aid[0] == 0) break;
            
            changed |= view.get_int("Value #", ot.amount_nr);
            changed |= view.get_float("Amount, %", ot.amount_pct, 2);
            changed |= view.get_float("Fixed amount", ot.amount_value, 2);
            if (static_cast<Model::IntType>(tt.operations.size()) > ix) {
                tt.operations[ix] = ot;
            } else {
                tt.operations.push_back(ot);
            }
            ix++;
        }
    } catch (const InputComplete& e) {
        return e.cancel ? false : changed;
    }
    return changed;
}

bool TransactionTemplates::get_account(Model::AccountId &value, Model::Account& c) {
    bool result=false;
    while (true) {
        try {
            result = view.get_text("Account", value, 8);
            if (value[0] == 0) break;
            if (strcmp(value,"-")==0) {
                value[0] = 0;
            } else {
                if (accounts_vm.found(value, c)) break;
                view.message("Invalid account Id (use /? to display list of columns)");
            }
        } catch (const InputLookup&) {
            accounts_vm.show_lookup();
        }
    }
    return result;
}

bool TransactionTemplates::handle(std::string command) {
    if (command.rfind("/a", 0) == 0) {
        view.message("Add a new template (use /; or empty value in the column to finish)");
        Model::TransactionTemplate transaction_template;
        if ( edit(transaction_template) ) {
            auto ix = storage.insert(transaction_template);
            view.message(">>> A new template added <<<");
            show( ix );
        } else {
            view.message(">>> Input canceled for a new template <<<");
        }
        return true;
    } else if (command.rfind("/e", 0) == 0) {
        view.message("Edit a template");
        Model::IntType ix = last_ix;
        Model::TransactionTemplate transaction_template;
        view.get_int("Enter Nr", ix, "[0-9]+");
        if (storage.find(ix, transaction_template)) {
            show( ix );
            if ( edit(transaction_template) ) {
                storage.update(transaction_template);
                view.message(">>> Template updated <<<");
                show( ix );
            } else {
                view.message(">>> Edit template canceled <<<");
            }
        } else {
            view.message(">>> Not found <<<");
        }
        return true;
    } else if (command.rfind("/d", 0) == 0) {
        view.message("Delete a template");
        Model::IntType ix=last_ix;
        Model::TransactionTemplate transaction_template;
        view.get_int("Enter Nr", ix, "[0-9]+");
        if (storage.find(ix, transaction_template)) {
            show( ix );
            if (view.confirm("Are you sure?")) {
                storage.remove(transaction_template);
                view.message(">>> Template deleted <<<");
            }
        } else {
            view.message(">>> Not found <<<");
        }
        return true;
    } else if (command == "/l" || command.length() == 0 || command == "/lt") {
        prompt = " Templates > ";
        list();
        return true;
    } else {
        return false;
    }
}

void TransactionTemplates::show_lookup() {
    view.table_header({"Id","Name","Expression"}, {8,25,25});
    for (auto tt : storage.select()) {
        view.table_item(tt.id, 8);
        view.table_item(tt.name, 25);
        view.table_item(tt.operations_as_str(), 25);
        view.table_eor();
    }
    view.table_footer({8,25,25});
}

bool TransactionTemplates::found(const Model::TemplateId id, Model::TransactionTemplate& value) {
    for (auto& e : storage.select()) {
        if ( strcmp(e.id,id) == 0 ) {
            value = e;
            return true;
        }
    }
    return false;
}

}
