//
//  columns.hpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#ifndef columns_vm_hpp
#define columns_vm_hpp

#include "viewmodel.hpp"
#include "../models/account_m.hpp"

namespace ViewModel {

class AccountsStorage : public Storage {
public:
    AccountsStorage(StorageRepository& aowner) : Storage(aowner) {}
    virtual Model::IntType insert(Model::Account& account) = 0;
    virtual bool update(Model::Account& account) = 0;
    virtual bool remove(Model::Account& account) = 0;
    virtual Model::AccountList& select() = 0;
    virtual bool find(Model::IntType key, Model::Account& account) = 0;
};

class Accounts : public ViewModel {
private:
    std::vector<std::string> id_list;
protected:
    Model::IntType last_ix;
    AccountsStorage& storage;
    void show_one(::Model::Account& account);
    void list();
    void show(Model::IntType ix);
    bool edit(Model::Account& account);
    void show_header();
    void show_footer();
public:
    Accounts(View& aview, AccountsStorage& astorage) : ViewModel(aview, astorage), storage(astorage) {}
    bool handle(std::string command) override;
    void show_lookup();
    bool found(const Model::AccountId id, Model::Account& value);
    std::vector<std::string>& get_list();
};

}

#endif /* columns_hpp */
