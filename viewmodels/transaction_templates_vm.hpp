//
//  transaction_templates.hpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#ifndef transaction_templates_vm_hpp
#define transaction_templates_vm_hpp

#include "viewmodel.hpp"
#include "../models/model.hpp"
#include "../models/template_m.hpp"
#include "accounts_vm.hpp"

namespace ViewModel {

class TransactionTemplatesStorage : public Storage {
public:
    TransactionTemplatesStorage(StorageRepository& aowner) : Storage(aowner) {};
    virtual Model::IntType insert(Model::TransactionTemplate& transaction_template) = 0;
    virtual bool update(Model::TransactionTemplate& transcation_template) = 0;
    virtual bool remove(Model::TransactionTemplate& transaction_template) = 0;
    virtual Model::TransactionTemplateList& select() = 0;
    virtual bool find(Model::IntType key, Model::TransactionTemplate& transaction_template) = 0;
};

class TransactionTemplates : public ViewModel {
protected:
    Model::IntType last_ix;
    TransactionTemplatesStorage& storage;
    Accounts& accounts_vm;
    void list();
    void show(Model::IntType ix);
    bool edit(Model::TransactionTemplate& transaction_template);
    bool get_account(Model::AccountId& value, Model::Account& c);
public:
    TransactionTemplates(View& aview, TransactionTemplatesStorage& astorage, Accounts& c) :
        ViewModel(aview, astorage), storage(astorage), accounts_vm(c) {};
    bool handle(std::string command) override;
    void show_lookup();
    bool found(const Model::TemplateId id, Model::TransactionTemplate& value);
};

}

#endif /* transaction_templates_hpp */
