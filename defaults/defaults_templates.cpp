//
//  defaults_templates.cpp
//  sea
//
//  Created by Jurijs Romanovs on 25/05/2021.
//

#include "defaults_templates.hpp"

void load_default_templates(ViewModel::TransactionTemplatesStorage& storage) {
    Model::TransactionTemplate tt;
    Model::OperationTemplate o;

    strcpy(tt.id, "1");
    tt.name = "Invoice to client";
    tt.day = 1;
    tt.document = "Upwork statement";
    tt.partner = "Client LLC";
    tt.description = "Invoice software developent";
    tt.amounts = 1;
    tt.amount_titles.push_back("The amount of invoice in USD");
    strcpy(o.aid, "upw");
    o.amount_nr = 1;
    o.amount_pct = 100;
    o.amount_value = 0;
    tt.operations.push_back(o);
    strcpy(o.aid, "upw");
    o.amount_nr = 1;
    o.amount_pct = -5;
    o.amount_value = 0;
    tt.operations.push_back(o);
    strcpy(o.aid, "inc");
    o.amount_nr = -1;
    o.amount_pct = 0;
    o.amount_value = 0;
    tt.operations.push_back(o);
    strcpy(o.aid, "exp");
    o.amount_nr = -2;
    o.amount_pct = 0;
    o.amount_value = 0;
    tt.operations.push_back(o);
    storage.insert(tt);

    tt.operations.clear();
    tt.amount_titles.clear();
    strcpy(tt.id, "2");
    tt.name = "Upwork to Swedbank";
    tt.day = 1;
    tt.document = "Upwork statement";
    tt.partner = "Freelancer Name";
    tt.description = "Withdrawal to the owner account";
    tt.amounts = 2;
    tt.amount_titles.push_back("Withdrawal amount in USD");
    tt.amount_titles.push_back("Withdrawal amount in EUR");
    strcpy(o.aid, "upw");
    o.amount_nr = 1;
    o.amount_pct = -100;
    o.amount_value = 0;
    tt.operations.push_back(o);
    strcpy(o.aid, "tra");
    o.amount_nr = 2;
    o.amount_pct = 100;
    o.amount_value = 0;
    tt.operations.push_back(o);
    strcpy(o.aid, "upw");
    o.amount_nr = 0;
    o.amount_pct = 0;
    o.amount_value = -2.00;
    tt.operations.push_back(o);
    strcpy(o.aid, "exp");
    o.amount_nr = -3;
    o.amount_pct = 0;
    o.amount_value = 0;
    tt.operations.push_back(o);
    storage.insert(tt);
    
    tt.operations.clear();
    tt.amount_titles.clear();
    strcpy(tt.id, "3");
    tt.name = "Swedbank comission";
    tt.day = 1;
    tt.document = "Swedbank statement";
    tt.partner = "Swedbank";
    tt.description = "monthly fee";
    tt.amounts = 0;
    strcpy(o.aid, "swd");
    o.amount_nr = 1;
    o.amount_pct = 0;
    o.amount_value = -3.54;
    tt.operations.push_back(o);
    strcpy(o.aid, "exp");
    o.amount_nr = 1;
    o.amount_pct = 0;
    o.amount_value = -3.54;
    tt.operations.push_back(o);
    storage.insert(tt);

    tt.operations.clear();
    tt.amount_titles.clear();
    strcpy(tt.id, "4");
    tt.name = "Swedbank from Upwork";
    tt.day = 25;
    tt.document = "Swedbank statement";
    tt.partner = "Freelancer Name";
    tt.description = "transfer from Upwork";
    tt.amounts = 1;
    tt.amount_titles.push_back("Transfer amount in EUR");
    strcpy(o.aid, "swd");
    o.amount_nr = 1;
    o.amount_pct = 100;
    o.amount_value = 0;
    tt.operations.push_back(o);
    strcpy(o.aid, "tra");
    o.amount_nr = 1;
    o.amount_pct = -100;
    o.amount_value = 0;
    tt.operations.push_back(o);
    storage.insert(tt);

    tt.operations.clear();
    tt.amount_titles.clear();
    strcpy(tt.id, "5");
    tt.name = "Swedbank to Freelancer";
    tt.day = 25;
    tt.document = "Swedbank statement";
    tt.partner = "Freelancer Name";
    tt.description = "owner withdrawal";
    tt.amounts = 1;
    tt.amount_titles.push_back("Withdrawal amount in EUR");
    strcpy(o.aid, "swd");
    o.amount_nr = 1;
    o.amount_pct = -100;
    o.amount_value = 0;
    tt.operations.push_back(o);
    storage.insert(tt);
}
