//
//  defaults_accounts.hpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#ifndef defaults_accounts_hpp
#define defaults_accounts_hpp

#include "../storage/flatfile_accounts.hpp"

void load_default_colummns(ViewModel::AccountsStorage& storage);

#endif /* defaults_accounts_hpp */
