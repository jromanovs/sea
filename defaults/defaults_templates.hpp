//
//  defaults_templates.hpp
//  sea
//
//  Created by Jurijs Romanovs on 25/05/2021.
//

#ifndef defaults_templates_hpp
#define defaults_templates_hpp

#include "../storage/flatfile_templates.hpp"

void load_default_templates(ViewModel::TransactionTemplatesStorage& storage);

#endif /* defaults_templates_hpp */
