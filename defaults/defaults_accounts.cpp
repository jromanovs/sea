//
//  defaults_columns.cpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#include "defaults_accounts.hpp"
#include "../storage/flatfile_accounts.hpp"

void load_default_colummns(ViewModel::AccountsStorage& storage) {
    Model::Account a;
    
    strcpy(a.id, "upw");
    a.title = "Upwork";
    a.kind = Model::Money;
    a.group = "Cash";
    strcpy(a.currency, "USD");
    storage.insert(a);
    
    strcpy(a.id, "tra");
    a.title = "Transfers";
    a.kind = Model::Money;
    a.group = "Cash";
    strcpy(a.currency, "EUR");
    storage.insert(a);
    
    strcpy(a.id, "swd");
    a.title = "Swedbank";
    a.kind = Model::Money;
    a.group = "Cash";
    strcpy(a.currency, "EUR");
    storage.insert(a);

    strcpy(a.id, "inc");
    a.title = "Income";
    a.kind = Model::Money;
    a.group = "Income";
    strcpy(a.currency, "EUR");
    storage.insert(a);

    strcpy(a.id, "exp");
    a.title = "Expenses";
    a.kind = Model::Money;
    a.group = "Expenses";
    strcpy(a.currency, "EUR");
    storage.insert(a);
}
