//
//  flatfile_base.hpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#ifndef flatfile_base_hpp
#define flatfile_base_hpp

const int transaction_storage_id=1;
const int operations_storage_id=2;
const int templates_storage_id=3;
const int templateoperations_storage_id=4;
const int column_storage_id=5;
const char eoh = 0xCA;
const char eos = 0xCE;

#endif /* flatfile_base_h */
