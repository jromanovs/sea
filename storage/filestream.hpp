//
//  filestream.hpp
//  sea
//
//  Created by Jurijs Romanovs on 30/04/2021.
//

#ifndef filestream_hpp
#define filestream_hpp

#include <fstream>
#include "../models/model.hpp"

class OutFileStream : public std::ofstream {
public:
    OutFileStream(const std::string& filename) : std::ofstream( filename, std::ios::binary ){};
    std::ostream& writestr(const std::string& s);
    std::ostream& writeulint(Model::IntType n);
};

class InFileStream : public std::ifstream {
public:
    InFileStream(const std::string& filename) : std::ifstream( filename, std::ios::binary ){};
    std::istream& readstr(std::string* s);
    std::istream& readulint(Model::IntType* n);
};

#endif /* filestream_hpp */
