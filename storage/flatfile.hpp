//
//  flatfile.hpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#ifndef flatfile_hpp
#define flatfile_hpp

#include "filestream.hpp"
#include "../viewmodels/viewmodel.hpp"
#include "../models/account_m.hpp"

#include "flatfile_accounts.hpp"
#include "flatfile_transactions.hpp"
#include "flatfile_templates.hpp"

class FlatFileStorage : public ViewModel::StorageRepository {
    std::string filename;
    FlatFileColumns columns;
    FlatFileTransactions transactions;
    FlatFileTemplates templates;
public:
    FlatFileStorage(std::string afilename) : filename(afilename),
      columns(*this), transactions(*this), templates(*this) {}
    ViewModel::AccountsStorage& get_columns() { return columns; }
    ViewModel::TransactionsStorage& get_transactions() { return transactions; }
    ViewModel::TransactionTemplatesStorage& get_templates() { return templates; }
    bool exists();
    int load();
    void save() override;
};

#endif /* flatfile_hpp */
