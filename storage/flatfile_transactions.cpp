//
//  flatfile_transactions.cpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#include "flatfile_transactions.hpp"
#include <regex>
#include <cstring>

Model::IntType FlatFileTransactions::insert(Model::Transaction& transaction) {
    clean0(transaction);
    transaction.pkey = ++tkey; //list.size() + 1;
    list.push_back(transaction);
    owner.save();
    return transaction.pkey;
}

bool FlatFileTransactions::update(Model::Transaction& transaction) {
    clean0(transaction);
    for (auto& e : list ) {
        if (e.pkey==transaction.pkey) {
            e = transaction;
            owner.save();
            return true;
        }
    }
    return false;
}

bool FlatFileTransactions::remove(Model::Transaction& transaction) {
    // todo: verify integrity
    for (auto it = list.begin(); it != list.end(); it++) {
        if (it->pkey==transaction.pkey) {
            it = list.erase(it);
            owner.save();
            return true;
        }
    }
    return false;
}

Model::TransactionList& FlatFileTransactions::select() {
    selection.clear();
    for (auto& e : list ) {
        selection.push_back(e);
    }
    std::sort(selection.begin(),selection.end(),[](auto& a, auto& b) {
        return strcmp(a.ymd,b.ymd)<0;
    });
    return selection;
}

Model::TransactionList& FlatFileTransactions::select(ViewModel::TransactionSearch& search) {
    selection.clear();
    for (auto& e : list ) {
        // e.ymd < search.period_end ...
        if (e.in_range(search.amount_begin, search.amount_end) &&
            (search.pattern.length()==0 ||
             std::regex_match(e.description, std::regex(search.pattern)) ||
             std::regex_match(e.partner, std::regex(search.pattern)) ||
             std::regex_match(e.document, std::regex(search.pattern))) &&
            (search.cid[0]==0 || e.found(search.cid)) &&
            strcmp(e.ymd,search.period_begin)>=0 &&
            strcmp(e.ymd,search.period_end)<=0) {
            selection.push_back(e);
        }
    }
    std::sort(selection.begin(),selection.end(),[](auto& a, auto& b) {
        return strcmp(a.ymd,b.ymd)<0;
    });
    return selection;
}

Model::TransactionList& FlatFileTransactions::select(Model::YearMonthDay ymd) {
    selection.clear();
    for (auto& e : list) {
        if ( strcmp(e.ymd,ymd)==0) {
            selection.push_back(e);
        }
    }
    return selection;
}

bool FlatFileTransactions::find(Model::IntType key, Model::Transaction& transaction) {
    for (auto& e : list) {
        if (e.pkey==key) {
            transaction = e;
            return true;
        }
    }
    return false;
}

void FlatFileTransactions::save(OutFileStream& os) {
    Model::IntType size = list.size();
    os.write(reinterpret_cast<char*>(&tkey), sizeof tkey);
    os.write(reinterpret_cast<char*>(&okey), sizeof okey);
    os.write(reinterpret_cast<char*>(&size), sizeof size);
    for (auto& e : list) {
        os.write(reinterpret_cast<char*>(&e.pkey), sizeof e.pkey);
        os.write(e.ymd, sizeof e.ymd);
        os.writestr(e.document);
        os.writestr(e.partner);
        os.writestr(e.description);
        os.write(e.tid, sizeof e.tid);
        size = e.operations.size();
        os.write(reinterpret_cast<char*>(&size), sizeof size);
        for (auto& o : e.operations) {
            os.write(reinterpret_cast<char*>(&o.pkey), sizeof o.pkey);
            os.write(o.aid, sizeof o.aid);
            os.write(reinterpret_cast<char*>(&o.amount), sizeof o.amount);
            os.write(reinterpret_cast<char*>(&o.rate), sizeof o.rate);
        }
    }
}

bool FlatFileTransactions::load(InFileStream& is) {
    Model::IntType size;
    is.read(reinterpret_cast<char*>(&tkey), sizeof tkey);
    is.read(reinterpret_cast<char*>(&okey), sizeof okey);
    is.read(reinterpret_cast<char*>(&size), sizeof size);
    while(size--) {
        Model::Transaction t;
        is.read(reinterpret_cast<char*>(&t.pkey), sizeof t.pkey);
        is.read(t.ymd, sizeof t.ymd);
        is.readstr(&t.document);
        is.readstr(&t.partner);
        is.readstr(&t.description);
        is.read(t.tid, sizeof t.tid);
        Model::IntType osize;
        is.read(reinterpret_cast<char*>(&osize), sizeof osize);
        while(osize--) {
            Model::Operation o;
            is.read(reinterpret_cast<char*>(&o.pkey), sizeof o.pkey);
            is.read(o.aid, sizeof o.aid);
            is.read(reinterpret_cast<char*>(&o.amount), sizeof o.amount);
            is.read(reinterpret_cast<char*>(&o.rate), sizeof o.rate);
            t.operations.push_back(o);
        }
        list.push_back(t);
    }
    return true;
    
}

void FlatFileTransactions::clean0(Model::Transaction& transaction) {
    for(auto it=transaction.operations.begin(); it!=transaction.operations.end(); )
    {
        if ((*it).amount == 0) {
            it = transaction.operations.erase(it);
        } else {
            it++;
        }
    }
}

