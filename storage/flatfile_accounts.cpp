//
//  flatfile_accounts.cpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#include "flatfile_accounts.hpp"
#include "../models/account_m.hpp"
#include "flatfile_base.hpp"

Model::IntType FlatFileColumns::insert(Model::Account& account) {
    account.pkey = ++ckey;
    list.push_back(account);
    owner.save();
    return account.pkey;
}

bool FlatFileColumns::update(Model::Account& account) {
    for (auto& e : list ) {
        if (e.pkey==account.pkey) {
            e = account;
            owner.save();
            return true;
        }
    }
    return false;
}

bool FlatFileColumns::remove(Model::Account& account) {
    for (auto it = list.begin(); it != list.end(); it++) {
        if (it->pkey==account.pkey) {
            it = list.erase(it);
            owner.save();
            return true;
        }
    }
    return false;
}

Model::AccountList& FlatFileColumns::select() {
    selection.clear();
    for (auto& e : list ) {
        selection.push_back(e);
    }
    return selection;
}

bool FlatFileColumns::find(Model::IntType key, Model::Account& column) {
    for (auto& e : list) {
        if (e.pkey==key) {
            column = e;
            return true;
        }
    }
    return false;
}

void FlatFileColumns::save(OutFileStream& os) {
    Model::IntType size = list.size();
    os.write(reinterpret_cast<char*>(&ckey), sizeof ckey);
    os.write(reinterpret_cast<char*>(&size), sizeof size);
    for (auto& e : list) {
        os.write(reinterpret_cast<char*>(&e.pkey), sizeof e.pkey);
        os.write(e.id, sizeof e.id);
        os.writestr(e.title);
        os.write(reinterpret_cast<char*>(&e.kind), sizeof e.kind);
        os.writestr(e.group);
        os.write(e.currency, sizeof e.currency);
    }
}

bool FlatFileColumns::load(InFileStream& is) {
    Model::IntType size;
    is.read(reinterpret_cast<char*>(&ckey), sizeof ckey);
    is.read(reinterpret_cast<char*>(&size), sizeof size);
    while(size--) {
        Model::Account a;
        is.read(reinterpret_cast<char*>(&a.pkey), sizeof a.pkey);
        is.read(a.id, sizeof a.id);
        is.readstr(&a.title);
        is.read(reinterpret_cast<char*>(&a.kind), sizeof a.kind);
        is.readstr(&a.group);
        is.read(a.currency, sizeof a.currency);
        list.push_back(a);
    }
    return true;
}
