//
//  filestream.cpp
//  sea
//
//  Created by Jurijs Romanovs on 30/04/2021.
//

#include "filestream.hpp"

std::istream& InFileStream::readstr(std::string* s) {
    unsigned long n;
    read(reinterpret_cast<char*>(&n), sizeof n);
    s->resize(n);
    const char* p = s->data();
    return read((char*)p, n);
}

std::istream& InFileStream::readulint(Model::IntType* n) {
    return read(reinterpret_cast<char*>(n), sizeof *n);
}

std::ostream& OutFileStream::writestr(const std::string& s) {
    unsigned long size = s.length();
    writeulint(size);
    return write(s.data(), size);
}

std::ostream& OutFileStream::writeulint(Model::IntType n) {
    return write(reinterpret_cast<char*>(&n), sizeof n);
}
