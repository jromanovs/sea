//
//  flatfile_accounts.hpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#ifndef flatfile_accounts_hpp
#define flatfile_accounts_hpp

#include "filestream.hpp"
#include "../viewmodels/accounts_vm.hpp"

class FlatFileColumns : public ViewModel::AccountsStorage {
    Model::AccountList list;
    Model::AccountList selection;
    Model::IntType ckey{};
public:
    FlatFileColumns(ViewModel::StorageRepository& aowner) : ViewModel::AccountsStorage(aowner) { };
    Model::IntType insert(Model::Account& column) override;
    bool update(Model::Account& column) override;
    bool remove(Model::Account& column) override;
    Model::AccountList& select() override;
    bool find(Model::IntType key, Model::Account& column) override;
    void save(OutFileStream& os);
    bool load(InFileStream& is);
};

#endif /* flatfile_accounts_hpp */
