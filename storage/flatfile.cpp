//
//  flatfile.cpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#include <iostream>
#include <fstream>
#include <cstring>
#include "flatfile.hpp"
#include "flatfile_base.hpp"

const char db_signature[3] = {'S','E','A'};

struct Header {
    const char db_signature[3];
    const char version_major;
    const char version_minor;
    const char number_of_structures;
    const char eoh;
};

bool FlatFileStorage::exists() {
    std::ifstream f(filename);
    return f.good();
}

int FlatFileStorage::load() {
    if (InFileStream is{filename}) {
        Header header{};
        is.read(reinterpret_cast<char*>(&header), sizeof header);
        if (!strcmp(header.db_signature, db_signature) || header.version_major > 1 || header.eoh != eoh) {
            std::cout << "*ERROR: Wrong db signature data was not loaded" << std::endl;
            return false;
        }
        
        while( is.peek() != EOF ) {
            int id;
            char end;
            is.read(reinterpret_cast<char*>(&id), sizeof id);
            if (id < 1 || id > header.number_of_structures ) {
                std::cout << "* ERROR: wrong number of structures" << std::endl;
                return 1;
            }
            switch (id) {
                case transaction_storage_id:
                    if(!transactions.load(is)) return 2;
                    break;
                case operations_storage_id:
                    break;
                case templates_storage_id:
                    if(!templates.load(is)) return 3;
                    break;
                case templateoperations_storage_id:
                    break;
                case column_storage_id:
                    if(!columns.load(is)) return 4;
                    break;
            }
            
            is.read(reinterpret_cast<char*>(&end), sizeof end);
            if (end != eos) {
                std::cout << "* ERROR: wrong end-of-structure tag" << std::endl;
                return 5;
            }
        }
    }
    return 0;
}

void FlatFileStorage::save() {
    Header default_header{{'S','E','A'},1,0,5,eoh};
    if (OutFileStream os{filename}) {
        os.write(reinterpret_cast<char*>(&default_header), sizeof default_header);

        os.write(reinterpret_cast<const char*>(&transaction_storage_id), sizeof transaction_storage_id);
        transactions.save(os);
        os.write(&eos, sizeof eos);

        os.write(reinterpret_cast<const char*>(&templates_storage_id), sizeof templates_storage_id);
        templates.save(os);
        os.write(&eos, sizeof eos);

        os.write(reinterpret_cast<const char*>(&column_storage_id), sizeof column_storage_id);
        columns.save(os);
        os.write(&eos, sizeof eos);
    }
    
}
