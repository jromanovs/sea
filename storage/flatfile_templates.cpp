//
//  flatfile_templates.cpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#include "flatfile_templates.hpp"
#include <algorithm>

Model::IntType FlatFileTemplates::insert(Model::TransactionTemplate& transaction_template) {
    clean0(transaction_template);
    transaction_template.pkey = ++ttkey;
    list.push_back(transaction_template);
    owner.save();
    return transaction_template.pkey;
}

bool FlatFileTemplates::update(Model::TransactionTemplate& transaction_template) {
    clean0(transaction_template);
    for (auto& e : list ) {
        if (e.pkey==transaction_template.pkey) {
            e = transaction_template;
            owner.save();
            return true;
        }
    }
    return false;
}

bool FlatFileTemplates::remove(Model::TransactionTemplate& transaction_template) {
    // todo: verify integrity
    for (auto it = list.begin(); it != list.end(); it++) {
        if (it->pkey==transaction_template.pkey) {
            it = list.erase(it);
            owner.save();
            return true;
        }
    }
    return false;
}

Model::TransactionTemplateList& FlatFileTemplates::select() {
    selection.clear();
    for (auto& e : list ) {
        selection.push_back(e);
    }
    std::sort(selection.begin(),selection.end(),[](auto& a, auto& b) {
        return strcmp(a.id,b.id)<0;
    });
    return selection;
}

bool FlatFileTemplates::find(Model::IntType key, Model::TransactionTemplate& transaction_template) {
    for (auto& e : list) {
        if (e.pkey==key) {
            transaction_template = e;
            return true;
        }
    }
    return false;
}

void FlatFileTemplates::save(OutFileStream& os) {
    Model::IntType size = list.size();
    os.write(reinterpret_cast<char*>(&ttkey), sizeof ttkey);
    os.write(reinterpret_cast<char*>(&tokey), sizeof tokey);
    os.write(reinterpret_cast<char*>(&size), sizeof size);
    for (auto& e : list) {
        os.write(reinterpret_cast<char*>(&e.pkey), sizeof e.pkey);
        os.write(e.id, sizeof e.id);
        os.writestr(e.name);
        os.write(reinterpret_cast<char*>(&e.day), sizeof e.day);
        os.writestr(e.document);
        os.writestr(e.partner);
        os.writestr(e.description);
        os.write(reinterpret_cast<char*>(&e.amounts), sizeof e.amounts);

        size = e.amount_titles.size();
        os.write(reinterpret_cast<char*>(&size), sizeof size);
        for (auto& t : e.amount_titles) {
            os.writestr(t);
        }

        size = e.operations.size();
        os.write(reinterpret_cast<char*>(&size), sizeof size);
        for (auto& o : e.operations) {
            os.write(reinterpret_cast<char*>(&o.pkey), sizeof o.pkey);
            os.write(o.aid, sizeof o.aid);
            os.write(reinterpret_cast<char*>(&o.amount_nr), sizeof o.amount_nr);
            os.write(reinterpret_cast<char*>(&o.amount_pct), sizeof o.amount_pct);
            os.write(reinterpret_cast<char*>(&o.amount_value), sizeof o.amount_value);
        }
    }
}

bool FlatFileTemplates::load(InFileStream& is) {
    Model::IntType size;
    is.read(reinterpret_cast<char*>(&ttkey), sizeof ttkey);
    is.read(reinterpret_cast<char*>(&tokey), sizeof tokey);
    is.read(reinterpret_cast<char*>(&size), sizeof size);
    while(size--) {
        Model::TransactionTemplate tt;
        is.read(reinterpret_cast<char*>(&tt.pkey), sizeof tt.pkey);
        is.read(tt.id, sizeof tt.id);
        is.readstr(&tt.name);
        is.read(reinterpret_cast<char*>(&tt.day), sizeof tt.day);
        is.readstr(&tt.document);
        is.readstr(&tt.partner);
        is.readstr(&tt.description);
        is.read(reinterpret_cast<char*>(&tt.amounts), sizeof tt.amounts);

        Model::IntType osize;
        is.read(reinterpret_cast<char*>(&osize), sizeof osize);
        while(osize--) {
            std::string value;
            is.readstr(&value);
            tt.amount_titles.push_back(value);
        }
        
        is.read(reinterpret_cast<char*>(&osize), sizeof osize);
        while(osize--) {
            Model::OperationTemplate ot;
            is.read(reinterpret_cast<char*>(&ot.pkey), sizeof ot.pkey);
            is.read(ot.aid, sizeof ot.aid);
            is.read(reinterpret_cast<char*>(&ot.amount_nr), sizeof ot.amount_nr);
            is.read(reinterpret_cast<char*>(&ot.amount_pct), sizeof ot.amount_pct);
            is.read(reinterpret_cast<char*>(&ot.amount_value), sizeof ot.amount_value);
            tt.operations.push_back(ot);
        }
        list.push_back(tt);
    }
    return true;
}

void FlatFileTemplates::clean0(Model::TransactionTemplate& transaction_template) {
    for(auto it=transaction_template.operations.begin(); it!=transaction_template.operations.end(); )
    {
        if ((*it).amount_nr == 0 && (*it).amount_value == 0) {
            it = transaction_template.operations.erase(it);
        } else {
            it++;
        }
    }
}

