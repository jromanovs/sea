//
//  flatfile_transactions.hpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#ifndef flatfile_transactions_hpp
#define flatfile_transactions_hpp

#include "filestream.hpp"
#include "../viewmodels/transactions_vm.hpp"

class FlatFileTransactions : public ViewModel::TransactionsStorage {
    Model::TransactionList list;
    Model::TransactionList selection;
    Model::IntType tkey{};
    Model::IntType okey{};
    void clean0(Model::Transaction& transaction);
public:
    FlatFileTransactions(ViewModel::StorageRepository& aowner) : ViewModel::TransactionsStorage(aowner) {};
    Model::IntType insert(Model::Transaction& transaction) override;
    bool update(Model::Transaction& transaction) override;
    bool remove(Model::Transaction& transaction) override;
    Model::TransactionList& select() override;
    Model::TransactionList& select(ViewModel::TransactionSearch& search) override;    
    Model::TransactionList& select(Model::YearMonthDay ymd) override;
    bool find(Model::IntType key, Model::Transaction& transaction) override;
    void save(OutFileStream& os);
    bool load(InFileStream& is);
};

#endif /* flatfile_transactions_hpp */
