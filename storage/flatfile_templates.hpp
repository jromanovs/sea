//
//  flatfile_templates.hpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#ifndef flatfile_templates_hpp
#define flatfile_templates_hpp

#include "filestream.hpp"
#include "../viewmodels/transaction_templates_vm.hpp"

class FlatFileTemplates : public ViewModel::TransactionTemplatesStorage {
    Model::TransactionTemplateList list;
    Model::TransactionTemplateList selection;
    Model::IntType ttkey{};
    Model::IntType tokey{};
    void clean0(Model::TransactionTemplate& transaction_template);
public:
    FlatFileTemplates(ViewModel::StorageRepository& aowner) : ViewModel::TransactionTemplatesStorage(aowner) {};
    Model::IntType insert(Model::TransactionTemplate& transaction_template) override;
    bool update(Model::TransactionTemplate& transaction_template) override;
    bool remove(Model::TransactionTemplate& transaction_template) override;
    Model::TransactionTemplateList& select() override;
    bool find(Model::IntType key, Model::TransactionTemplate& transaction_template) override;
    void save(OutFileStream& os);
    bool load(InFileStream& is);
};

#endif /* flatfile_templates_hpp */
