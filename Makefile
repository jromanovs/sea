vpath %.cpp defaults models storage view viewmodels

CC       = g++
CFLAGS   = -Wall -std=gnu++17 -c
LDFLAGS  = -lz

BUILDDIR = build
PRODUCT  = sea

SRC =  main.cpp
SRC += $(wildcard defaults/*.cpp)
SRC += $(wildcard models/*.cpp)
SRC += $(wildcard storage/*.cpp)
SRC += $(wildcard view/*.cpp)
SRC += $(wildcard viewmodels/*.cpp)

OBJ = $(patsubst %.cpp,$(BUILDDIR)/%.o,$(notdir $(SRC)))

.PHONY: all clean

all: $(BUILDDIR)/$(PRODUCT)

$(BUILDDIR)/$(PRODUCT): $(BUILDDIR) $(OBJ)
	$(CC) $(LDFLAGS) $(OBJ) -o $@

$(BUILDDIR):
	mkdir -p $@

$(BUILDDIR)/%.o: %.cpp
	$(CC) $(CFLAGS) $< -o $@

INCLUDES = -Idefaults -Imodels -Istorage -Iview -Iviewmodels

clean:
	-rm $(BUILDDIR)/*.o $(BUILDDIR)/sea
