//
//  transaction_m.hpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#ifndef transaction_m_hpp
#define transaction_m_hpp

#include <string>
#include <vector>
#include <cmath>
#include "model.hpp"
#include "template_m.hpp"
#include "account_m.hpp"

namespace Model {

struct Transaction;

struct Operation : public Model {
    Operation() {
        aid[0] = 0;
        amount = 0;
        rate = 0;
    };
    AccountId aid;
    double amount;
    double rate;
    double amount_default() { return round(amount*rate*100)/100; }
};

typedef std::vector<Operation> OperationList;

struct Transaction : public Model {
    Transaction() {
        int2ymd(current_year(), current_month(), current_day(), ymd);
        tid[0] = 0;
    };
    YearMonthDay ymd;
    std::string document;
    std::string partner;
    std::string description;
    TemplateId tid;
    OperationList operations;
    double sum(const AccountId cid) {
        double total=0;
        for (auto& o : operations) {
            if (strcmp(o.aid,cid)==0) {
                total += o.amount;
            }
        }
        return total;
    }
    double db(const AccountId cid) {
        double total=0;
        for (auto& o : operations) {
            if (strcmp(o.aid,cid)==0 && o.amount>0) {
                total += o.amount;
            }
        }
        return total;
    }
    double cr(const AccountId cid) {
        double total=0;
        for (auto& o : operations) {
            if (strcmp(o.aid,cid)==0 && o.amount<0) {
                total -= o.amount;
            }
        }
        return total;
    }
    double sum_rate(const AccountId cid) {
        double total=0;
        for (auto& o : operations) {
            if (strcmp(o.aid,cid)==0) {
                total += o.amount_default();
            }
        }
        return total;
    }
    double dbr(const AccountId cid) {
        double total=0;
        for (auto& o : operations) {
            if (strcmp(o.aid,cid)==0 && o.amount>0) {
                total += o.amount_default();
            }
        }
        return total;
    }
    double crr(const AccountId cid) {
        double total=0;
        for (auto& o : operations) {
            if (strcmp(o.aid,cid)==0 && o.amount<0) {
                total -= o.amount_default();
            }
        }
        return total;
    }
    bool found(AccountId cid) {
        bool result = false;
        for (auto& o : operations) {
            if (strcmp(o.aid,cid)==0) {
                result = true;
                break;
            }
        }
        return result;
    }
    bool in_range(double amount_begin, double amount_end) {
        bool result = false;
        for (auto& o : operations) {
            if (o.amount >= amount_begin && o.amount <= amount_end) {
                result = true;
                break;
            }
        }
        return result;
    }
};

typedef std::vector<Transaction> TransactionList;

}

#endif /* transaction_m_hpp */
