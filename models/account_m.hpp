//
//  accounts.hpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#ifndef account_m_hpp
#define account_m_hpp

#include <string>
#include <cstring>
#include <vector>
#include "model.hpp"

namespace Model {

typedef char AccountId[9];
typedef char Currency[4];

enum AccountKind : char { Money='M', Debt='D', Income='I', Expense='E' };

struct Account : public Model {
    Account() {
        id[0] = 0;
        kind = Money;
        strncpy(currency,"EUR",4);
    };
    AccountId id;
    std::string title;
    AccountKind kind;
    std::string group;
    Currency currency;
    bool is_default_currency() {
        return (strcmp(currency,default_currency)==0);
    }
    std::string kind_as_string() {
        switch (kind) {
            case Money:
                return std::string("Money");
                break;
            case Debt:
                return std::string("Debt");
                break;
            case Income:
                return std::string("Income");
                break;
            case Expense:
                return std::string("Expense");
                break;
            default:
                return std::string();
                break;
        }
    }
};

typedef std::vector<Account> AccountList;
}

#endif /* account_m_hpp */
