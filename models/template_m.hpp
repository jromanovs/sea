//
//  template_m.hpp
//  sea
//
//  Created by Jurijs Romanovs on 10/05/2021.
//

#ifndef template_m_hpp
#define template_m_hpp

#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include "model.hpp"
#include "account_m.hpp"

namespace Model {

typedef char TemplateId[9];

struct TransactionTemplate;

struct OperationTemplate : public Model {
    OperationTemplate() {
        aid[0] = 0;
        amount_nr = 1;
        amount_pct = 100;
        amount_value = 0;
    };
    AccountId aid;
    IntType amount_nr;
    double amount_pct;
    double amount_value;
};

typedef std::vector<OperationTemplate> OperationTemplateList;

struct TransactionTemplate : public Model {
    TransactionTemplate() {
        id[0] = 0;
        day = 1;
        amounts = 1;
    };
    TemplateId id;
    std::string name;
    IntType day;
    std::string document;
    std::string partner;
    std::string description;
    IntType amounts;
    std::vector<std::string> amount_titles;
    OperationTemplateList operations;
    std::string operations_as_str() {
        std::string result;
        for (auto& o:operations) {
            std::ostringstream os;
            os << o.aid << ":" <<
            std::fixed << std::setprecision(1) << o.amount_pct << "%" <<
            "-"<< o.amount_nr << " ";
            result += os.str();
        }
        return result;
    }
};

typedef std::vector<TransactionTemplate> TransactionTemplateList;

}

#endif /* template_m_hpp */
