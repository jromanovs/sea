//
//  base.cpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#include "model.hpp"
#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <regex>
#include <cstring>

namespace Model {

int current_year() {
    time_t now = time(0);
    tm *gmtm = localtime(&now);
    return gmtm->tm_year+1900;
}

int current_month() {
    time_t now = time(0);
    tm *gmtm = localtime(&now);
    return gmtm->tm_mon+1;
}

int current_day() {
    time_t now = time(0);
    tm *gmtm = localtime(&now);
    return gmtm->tm_mday;
}

void ymd2int(YearMonthDay ymd, int& year, int& month, int& day) {
    std::string y{ymd[0],ymd[1],ymd[2],ymd[3]};
    std::string m{ymd[4],ymd[5]};
    std::string d{ymd[6],ymd[7]};
    try {
        year = std::stoi(y);
    } catch(...) {
        year = 0;
    }
    try {
        month = std::stoi(m);
    } catch (...) {
        month = 0;
    }
    try {
        day = std::stoi(d);
    } catch (...) {
        day = 0;
    }
}

void int2ymd(int year, int month, int day, YearMonthDay& ymd) {
    std::ostringstream formatted;
    formatted
    << std::setw(4) << std::setfill('0') << year
    << std::setw(2) << std::setfill('0') << month
    << std::setw(2) << std::setfill('0') << day;
    strncpy(ymd,formatted.str().c_str(),sizeof ymd);
}


bool str2ymd(std::string& str, YearMonthDay& ymd) {
    bool result = false;
    int y,m,d;
    ymd2int(ymd, y, m, d);
    if (y==0) {
        y = current_year();
    }
    if (m==0) {
        m = current_month();
    }
    if (d==0) {
        d = current_day();
    }
    
    std::smatch sm;
    if (std::regex_match(str, sm, std::regex("([0-9]{1,2})([0-9]{1,2})?([0-9]{2,4})?"))) {
        for (IntType i=1; i < static_cast<IntType>(sm.size()); i++) {
            if(sm[i].length()!=0) {
                if (i==1) {
                    d = std::stoi(sm[i]);
                }
                if (i==2) {
                    m = std::stoi(sm[i]);
                }
                if (i==3) {
                    y = std::stoi(sm[i]);
                    if (y<100) y+=2000;
                }
            }
            
        }
        std::tm tm{};
        tm.tm_mday = d;
        tm.tm_mon = m-1;
        tm.tm_year = y-1900;
        auto t = std::mktime(&tm);
        auto new_t = std::localtime(&t);
        if (new_t->tm_year==y-1900 && new_t->tm_mon==m-1 && new_t->tm_mday==d) {
            std::ostringstream formatted;
            formatted
            << std::setw(4) << std::setfill('0') << y
            << std::setw(2) << std::setfill('0') << m
            << std::setw(2) << std::setfill('0') << d;
            strncpy(ymd, formatted.str().c_str(), 8);
            result = true;
        }
    };
    return result;
}

std::string ymd_format(YearMonthDay& ymd) {
    int y,m,d;
    std::ostringstream formatted;
    ymd2int(ymd, y, m, d);
    formatted
    << std::setw(2) << std::setfill('0') << d << "."
    << std::setw(2) << std::setfill('0') << m << "."
    << std::setw(4) << std::setfill('0') << y;
    return formatted.str();
}

std::string ymd2str(YearMonthDay& ymd) {
    int y,m,d;
    std::ostringstream formatted;
    ymd2int(ymd, y, m, d);
    formatted
    << std::setw(2) << std::setfill('0') << d
    << std::setw(2) << std::setfill('0') << m
    << std::setw(4) << std::setfill('0') << y;
    return formatted.str();
}

std::string month(int n)
{
    std::string months[] =
    { " January     + ", " February    + ", " March       + ",
      " April       + ", " May         + ", " June        + ",
      " July        + ", " August      + ", " September   + ",
      " October     + ", " November    + ", " December    + " };
    return (months[n]);
}

}
