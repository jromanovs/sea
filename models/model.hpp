//
//  basemodel.hpp
//  sea
//
//  Created by Jurijs Romanovs on 01/05/2021.
//

#ifndef model_hpp
#define model_hpp

#include <string>

namespace Model {

const char default_currency[] = "EUR";

typedef signed long IntType;

typedef char YearMonthDay[9];

struct Model {
    IntType pkey;
};

class ModelOperations {
};

int current_year();
int current_month();
int current_day();
std::string month(int n);

void ymd2int(YearMonthDay ymd, int& year, int& month, int& day);
void int2ymd(int year, int month, int day, YearMonthDay& ymd);
bool str2ymd(std::string& str, YearMonthDay& ymd);
std::string ymd_format(YearMonthDay& ymd);
std::string ymd2str(YearMonthDay& ymd);

}

#endif /* base_hpp */
